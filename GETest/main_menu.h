#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <game_engine.h>
#include <QList>

class MainMenu : public Layer
{
    Q_OBJECT

public:
    static Scene* scene();
    explicit MainMenu();
//    void onEnterTransitionDidFinish();
//    void onExitTransitionDidFinish();
    void onEnterTransitionDidStart();
    void onEnterTransitionDidFinish();
    void onExitTransitionDidStart();
    void onExitTransitionDidFinish();

public slots:
    void update(double delta);
    void animation();

private:
    QRect m_winSize;
    qreal m_backGroundHeight;
    Sprite *m_background;
    QList<Sprite*> m_cards;
    int m_counter;
};

#endif // MAIN_MENU_H
