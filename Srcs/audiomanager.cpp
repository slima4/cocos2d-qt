#include "audiomanager.h"
/*
Refer to
https://projects.developer.nokia.com/qtgameenabler/wiki/audio
for more info, please use wav 22050 kHz, 2 channels, signed 16 bit PCM.
*/

GE::AudioOut * AudioManager::m_audioOut;
GE::AudioMixer AudioManager::m_mixer;
#ifdef Q_OS_SYMBIAN
QTimer audioManager::m_audioPullTimer;
#endif

AudioManager::AudioManager(QObject *parent) :
    QObject(parent)
{

}

void AudioManager::playSound(QString path) {
    if (m_audioOut->state() != GE::AudioOut::NotRunning)
    {
        GE::AudioBuffer* m_someSample = GE::AudioBuffer::loadWav(path);
        GE::AudioBufferPlayInstance *m_instance = new GE::AudioBufferPlayInstance(m_someSample);
        connect(m_instance,SIGNAL(finished()),m_someSample,SLOT(deleteLater()));
    //    connect(m_instance,SIGNAL(finished()),m_instance,SLOT(deleteLater()));
        m_mixer.addAudioSource(m_instance);
    }
}

void AudioManager::setUpAudioManager() {
    m_audioOut = new GE::AudioOut(&m_mixer);
#ifdef Q_OS_SYMBIAN
    m_audioPullTimer.setInterval(5);
    connect(&m_audioPullTimer, SIGNAL(timeout()), m_audioOut, SLOT(tick()));
#endif
    if (m_audioOut->state() != GE::AudioOut::NotRunning)
        AudioManager::enableSounds(true);
}

void AudioManager::enableSounds(bool enable)
{
    if (m_audioOut->state() == GE::AudioOut::NotRunning)
        return;

    if (enable) {
#ifdef Q_OS_SYMBIAN
        m_audioPullTimer.start();
#endif
        m_mixer.setGeneralVolume(0.2f);
    }
    else {
#ifdef Q_OS_SYMBIAN
        m_audioPullTimer.stop();
#endif
        m_mixer.setGeneralVolume(0);
    }
}
