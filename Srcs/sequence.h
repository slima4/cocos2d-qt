#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <stdarg.h> //va_list, va_start, va_arg, va_end
#include "action.h"

class Node;
class Action;

class Sequence : public QObject
{
    Q_OBJECT
public:
    static Sequence* sequenceWithActions(Action* Action,...);
    void setTarget(Node *target);

public slots:
    void oneActionisDone();

private:
    explicit Sequence();
    void runNextAction();

private:
    QList <Action*> m_actionList;
    Node *m_targetNode;


};

#endif // SEQUENCE_H
