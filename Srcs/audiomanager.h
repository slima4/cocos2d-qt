#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H

#include <QObject>
#include <QTimer>
#include "GEAudioMixer.h"
#include "GEAudioBuffer.h"
#include "GEAudioOut.h"
#include "GEAudioBufferPlayInstance.h"

class AudioManager : public QObject
{
    Q_OBJECT
public:
    static void playSound(QString path);
    static void setUpAudioManager();
    static void enableSounds(bool);

private:
    explicit AudioManager(QObject *parent = 0);
    static GE::AudioOut *m_audioOut;
    static GE::AudioMixer m_mixer;
#ifdef Q_OS_SYMBIAN
    static QTimer m_audioPullTimer; // Used to tick the audio engine in symbian devices only
#endif
};

#endif // AUDIOMANAGER_H
