#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsView>
#include <QPaintEvent>

class View :public QGraphicsView
{
    Q_OBJECT
public:
    View(QRect deskSize);

protected:
    void paintEvent ( QPaintEvent * event );

private:
    QRect m_winSize;
};

#endif // VIEW_H
