#include "sequence.h"

Sequence::Sequence() {
}

Sequence* Sequence::sequenceWithActions(Action* actionItem,...) {

    if (actionItem != NULL) {
        Sequence* newSeq = new Sequence;
        newSeq->m_actionList.append(actionItem);
        Action* addAction;
        va_list list;
        va_start(list,actionItem);
        addAction = va_arg(list, Action*);
        while (addAction != NULL) {
        newSeq->m_actionList.append(addAction);
        addAction = va_arg(list, Action*);
        }
        va_end(list);
        return newSeq;
    }
    else {
        qDebug() << "Game Engine :: Warning : you are trying to construct an empty sequence action, it will return for you a NULL pointer";
        return NULL;
    }

}

void Sequence::setTarget(Node *target) {
    m_targetNode = target;
    runNextAction();
}

void Sequence::oneActionisDone() {
    runNextAction();
}

void Sequence::runNextAction() {
    if (!m_actionList.isEmpty()) {
        Action* nextAction = m_actionList.takeFirst();
        connect(nextAction,SIGNAL(animationFinished(Action*)),this,SLOT(oneActionisDone()));
        m_targetNode->runAction(nextAction);
    }
    else {
        delete this;
    }
}
