#ifndef TRANSITIONSCENE_H
#define TRANSITIONSCENE_H
#include <QObject>
#include "scene.h"
#include "director.h"
#include "action.h"
#include "sequence.h"

class TransitionScene : public QObject
{
    Q_OBJECT

public:
    static TransitionScene* FadeTransition(Scene* otherScene,float duration);
    void setCurrScene(Scene* scene);

signals:
    void transitionDoneSignal(Scene* scene);

public slots:
    void FadeTransition_half_done();
    void transition_done();

private:
    TransitionScene();

private:
    Scene* m_currScene;
    Scene* m_nextScene;
    float m_dur;
    qreal m_origFade;
};

#endif // TRANSITIONSCENE_H
