#include "fileutils.h"
#include <QCoreApplication>
#include <QDir>

static FileUtils * g_pFileUtils = NULL;

FileUtils *FileUtils::shareFileUtils()
{
    if (g_pFileUtils == NULL)
    {
        g_pFileUtils = new FileUtils();
    }
    return g_pFileUtils;
}

void FileUtils::setResourceDirectory(const QString &directoryName)
{
    //this code, until the last QDir is to set the CWD, aka Current Working Directory
    QDir dir(QCoreApplication::applicationDirPath());
    if (dir.dirName().toLower() == "bin")
    {
        dir.cdUp();
    }
#ifdef WIN32
    if (dir.dirName().toLower() == "debug" || dir.dirName().toLower() == "release")
    {
        dir.cdUp();
    }
#endif

    m_Directory = directoryName;
    if (m_Directory.size() > 0 && !m_Directory.endsWith('/'))
    {
        m_Directory.append("/");
    }

    QDir::setCurrent(dir.absolutePath()+"/"+m_Directory);
}

QString &FileUtils::getResourceDirectory()
{
    return m_Directory;
}

FileUtils::FileUtils()
{
}
