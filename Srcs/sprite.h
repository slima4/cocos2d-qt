#ifndef SPRITE_H
#define SPRITE_H
#include "node.h"
#include "spriteitem.h"

class Sprite : public Node {

private:

public:
    static Sprite* spriteWithImage(const char* filename);
    ~Sprite();

private:
    Sprite(const QString &filename);
    SpriteItem* item;
    QGraphicsScene* getSharedGraphicScene(){ return 0; }
    void setSharedGraphicScene(QGraphicsScene* graphicscene){ return; }
    qreal getwidth();
    qreal getheight();
    QGraphicsItem* getGraphicsItem(){ return Node::getGraphicsItem(); }
    void setGraphicsItem(QGraphicsItem* item) { return;}
    void setW(qreal w) {}
    void setH(qreal h) {}

private:
    QPointF m_transformPoint;
};

#endif // SPRITE_H
