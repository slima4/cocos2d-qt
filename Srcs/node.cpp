#include "node.h"
#include "qmath.h"

#define pi 3.1415926
#define degtorad pi/180
#define radtodeg 180/pi


qreal pointDistance(QPointF point1,QPointF point2) {
    qreal x = point1.x() - point2.x();
    x *= x;
    qreal y = point1.y() - point2.y();
    y *= y;
    y += x;
    y = sqrt(y);
    return y;
}


int Node::m_global_max_z = 0;
QGraphicsScene* Node::m_sharedGraphicScene = 0;

Node::Node() {
    m_parent = 0;
    m_graphicsItem = 0;
    m_centerX = 0;
    m_centerY = 0;
    m_scaleFactor = 1.0;
    m_zOrder = 0;
    m_max_local_z = 0;
    m_width = 0;
    m_height = 0;
    m_updatescheduled = 0;
    m_rotaionAngle = 0;
    m_opacityValue = 100;
    m_isTouchEnabled = false;
}

Node::~Node() {

    // before deleting the item, we'll stop all the actions running on that item!
    stopAllActions();
    // if our nodes includes a graphical item, then yes, we'll remove it from the scene it belongs to
    if (m_graphicsItem != 0) {
        // we are checking if it's actually belongs to a scene
        if (m_graphicsItem->scene() != 0) {
            m_graphicsItem->scene()->removeItem(m_graphicsItem);
        }
        else {
            qDebug() << "GameEngine :: Warning : deleting an item, which actually doesn't belong to a scene";
        }
        delete m_graphicsItem;
        m_graphicsItem = 0;
    }
    // we'll recursively delete the childs one by one
    Node* child;
    while (!m_children.isEmpty()) {
        child = m_children.takeFirst();
        child->deleteLater();
    }
    if (m_parent == 0) {
        m_global_max_z = 0;
    }
}

qreal Node::opacity() const {
    return m_opacityValue;
}

qreal Node::realTimeOpacity() const {
    if (m_parent != 0) {
        return (m_opacityValue/100.0) * m_parent->realTimeOpacity();
    }
    return (m_opacityValue/100.0);
}

void Node::setOpacity(qreal value) {
    m_opacityValue = value;
    if (m_graphicsItem != 0) {
        m_graphicsItem->setOpacity(realTimeOpacity());
    }
    else {
        emit updateChildsOpacity();
    }
}

void Node::updateMyOpacity() {
    setOpacity(opacity());
}

void Node::setRotation(qreal rotationDegree) {
    m_rotaionAngle = rotationDegree;
    if (m_graphicsItem != 0) {
        //it's a graphics item not a layer nor a scene
        m_graphicsItem->setRotation((-realTimeRotation()));
        int realrotation = m_graphicsItem->rotation();
        if (realrotation%90 == 0) {
            //disable antianalising
        }
        else {
            //enable antianalising
        }
    }
    else {
        emit updateChildsRotation();
    }
    updateMyPos();

}
qreal Node::rotation() {
    return m_rotaionAngle;
}
qreal Node::realTimeRotation() const {
    if (m_parent!=0)
        return m_rotaionAngle + m_parent->realTimeRotation();
    return m_rotaionAngle;
}
void Node::updateMyRotation() {
    setRotation(rotation());
}

void Node::setPos(qreal x,qreal y) {
    setPos(QPointF(x,y));
}

void Node::setPos(const QPointF &pos)
{
    //here we are setting the position of our node according to it's parent, and it's parameter is the center point
    m_centerX = pos.x();
    m_centerY = pos.y();
    if (m_graphicsItem != 0) {
        //it's a graphics item not a layer nor a scene we'll set that to the new position
        m_graphicsItem->setPos(realTimePos());
    }
    else {
        emit updateChildsPos();
    }
}

void Node::setScale(qreal factor)
{
    m_scaleFactor = factor;
    if (m_graphicsItem != 0) {
        //it's a graphics item not a layer nor a scene
        qreal realScale = realTimeScale();
        m_graphicsItem->setScale(realScale);
    }
    else {
        emit updateChildsScale();
    }
    updateMyPos();
}

QPointF Node::realTimePos() const {
    qreal scalew = 0;
    qreal scaleh = 0;
    qreal rotatedx = m_centerX;
    qreal rotatedy = m_centerY;
    qreal moveByX = 0;
    qreal moveByY = 0;
    qreal distance = 0;
    qreal xangleOnCircle = 0;
    if (m_parent != 0) {
        //find the rotated position
        if (rotatedx != 0 || rotatedy != 0) {
            distance = pointDistance(QPointF(0,0),QPointF(rotatedx,rotatedy));
            // we want to move in a circule so we'll try to find what angle x lies on
            // using this know equation x = cx + r * cos(a)
            // our cx = 0,0 so cos(a) = x/r or the distance, we'll get the inverse
            xangleOnCircle;
            if (rotatedy > 0) {
                xangleOnCircle = rotatedy/distance;
            } else {
                xangleOnCircle = -rotatedy/distance;
            }
            xangleOnCircle = asin(xangleOnCircle);
            xangleOnCircle *= radtodeg;
            xangleOnCircle += m_parent->realTimeRotation();
            xangleOnCircle *= degtorad;

            // now we'll use this angle, and the distance, to find the point in out circle
            if (rotatedx < 0) {
                rotatedx = (distance*cos(xangleOnCircle)) * -1;
            }
            else {
                rotatedx = distance*cos(xangleOnCircle);
            }
            if (rotatedy < 0) {
                rotatedy = (distance*sin(xangleOnCircle)) * -1;
            }
            else {
                rotatedy = distance*sin(xangleOnCircle);
            }



        }

    }

    if (m_graphicsItem != 0) {


        scalew=(m_width*m_graphicsItem->scale())/2;
        scaleh=(m_height*m_graphicsItem->scale())/2;

        //that was the new point for the layer, now we'll center the item iteself,so rotation is done around center
        distance = pointDistance(QPointF(0,0),QPointF(scalew,scaleh));
        // here, we already have the rotation of the item
        xangleOnCircle = ((m_height*m_graphicsItem->scale())/2)/distance;
        xangleOnCircle = asin(xangleOnCircle);
        //convert it
        xangleOnCircle *= radtodeg;
        xangleOnCircle = 180.0 - xangleOnCircle;
        xangleOnCircle -= m_graphicsItem->rotation();
        xangleOnCircle *= degtorad;

        //now we just have to find the new diffrence
        moveByX = distance*cos(xangleOnCircle);
        moveByY = distance*sin(xangleOnCircle);

        moveByX = scalew + moveByX;
        moveByY = moveByY - scaleh;

    }
    if (m_parent != 0) {
        QPointF realtimePoint = m_parent->realTimePos();
        qreal realscale = m_parent->realTimeScale();
        return QPointF(realtimePoint.x() + ((rotatedx) * realscale) - scalew + moveByX ,realtimePoint.y() - (rotatedy* realscale) - scaleh - moveByY);
    }
    else {
        return QPointF(m_centerX,m_centerY);
    }
}

qreal Node::realTimeScale() const {
    if (m_parent != 0)
        return m_scaleFactor*m_parent->realTimeScale();
    return m_scaleFactor;
}

QPointF Node::pos() const {
    return QPointF(m_centerX,m_centerY);
}

qreal Node::scale() {
    return m_scaleFactor;
}
int Node::realTimeZ() const {
    if (m_parent != 0) {
        //am not the grand parent
        return m_parent->realTimeZ() + m_zOrder;
    }
    //here if am the grand parent, basically I'll return my Z level
    return m_zOrder + m_global_max_z;
}

void Node::setZ(int zValue) {
    m_zOrder = zValue;
    if (m_graphicsItem != 0) {
        m_graphicsItem->setZValue(realTimeZ());
    }
    else {
        emit updateChildsZ();
    }
}
void Node::updateMyPos() {
    setPos(pos());
}

void Node::updateMyScale() {
    setScale(scale());
}

void Node::updateMyZ() {
    setZ(z());
}



Action* Node::runAction(Action* actionPara) {
    actionPara->setTarget(this);
    m_actions.append(actionPara);
    return actionPara;
}

void Node::actionDone(Action* doneAction) {
    bool found = false;
    for (int i=0;i<=m_actions.size()&&!found;i++) {
        if(m_actions.at(i) == doneAction) {
            found = true;
            m_actions.removeAt(i);
            delete doneAction;
        }
    }
}


void Node::stopAction(Action* stopAction) {
    bool found = false;
    for (int i=0;i<m_actions.size()&&!found;i++) {
        if(m_actions.at(i) == stopAction) {
            found = true;
            stopAction->stopAnimation();
            m_actions.removeAt(i);
            delete stopAction;
        }
    }
}

void Node::stopAllActions() {
    Action* tempaction;
    while (!m_actions.isEmpty()) {
        tempaction = m_actions.takeFirst();
        tempaction->stopAnimation();
        delete tempaction;
    }
}

QGraphicsScene * Node::getSharedGraphicScene() {
    return m_sharedGraphicScene;
}

void Node::setSharedGraphicScene(QGraphicsScene *graphicscene) {
    m_sharedGraphicScene = graphicscene;
}

int Node::getGlobalMaxZ() {
    return m_global_max_z;
}

void Node::setGraphicsItem(QGraphicsItem *item) {
    m_graphicsItem = item;
}

QGraphicsItem * Node::getGraphicsItem() {
    return m_graphicsItem;
}

void Node::setW(qreal ww) {
    m_width = ww;
}

void Node::setH(qreal hh) {
    m_height = hh;
}

void Node::schedualUpdate() {
    if (!m_updatescheduled) {
        m_updatescheduled = 1;
        connect(Director::sharedDirector(),SIGNAL(update(double)),this,SLOT(update(double)));
    }
    else {
        qDebug() << "Game Engine :: you cannot Schedual an update that is already schedualed!!";
    }
}

void Node::unSchedualUpdate() {
    if (m_updatescheduled) {
        m_updatescheduled = 0;
        disconnect(Director::sharedDirector(),SIGNAL(update(double)),this,SLOT(update(double)));
    }
    else {
        qDebug() << "Game Engine :: you cannot unSchedual an update that is already unschedualed!!";
    }
}

void Node::update(double delta) {
    qDebug() << "Game Engine :: to schedual an update, please overload this methode as a public slot, \"void ClassName::update(double delta)\", delta is the time elapsed in seconds since last update, with milliseconds after decimal points, this methode is called at every frame, now this will unschedual itself!!";
    unSchedualUpdate();
}

void Node::runSequence(Sequence *sequencePara) {
    sequencePara->setTarget(this);
}

qreal Node::width() {
    return m_width;
}

qreal Node::height() {
    return m_height;
}

int Node::z() {
    return m_zOrder;
}

//child managment methodes

void Node::addChild(Node *child, int tag, int z) {
    // I'm this child parent!!
    child->m_parent = this;
    //I'll add this child to my childs list
    this->m_children.append(child);
    //I'll also set it's tag
    child->m_nodetag = tag;
    // setting the Z
    child->setZ(child->z());
    child->setPos(child->pos());
    child->setScale(child->scale());
    connect(this,SIGNAL(updateChildsPos()),child,SLOT(updateMyPos()));
    connect(this,SIGNAL(updateChildsScale()),child,SLOT(updateMyScale()));
    connect(this,SIGNAL(updateChildsZ()),child,SLOT(updateMyZ()));
    connect(this,SIGNAL( updateChildsRotation() ),child,SLOT( updateMyRotation() ) );
    connect(this,SIGNAL( updateChildsOpacity() ),child,SLOT( updateMyOpacity() ) );
    if (child->m_graphicsItem != 0) {
        // if this is actually some item, ie, not a layer nor a scene, we'll add it to the view
        m_sharedGraphicScene->addItem(child->m_graphicsItem);
    } else {
    }
}

Node* Node::getChildByTag(int tag) {
    bool found = false;
    Node *child = 0;
    for (int i=0; !found && i<m_children.size() ;i++) {
        child = m_children.at(i);
        if(child->m_nodetag == tag) {
            found = true;
        }
    }
    return child;
}

void Node::removeChildByTag(int tag) {
    bool found = false;
    Node *child = 0;
    for (int i=0; !found && i<m_children.size() ;i++) {
        child = m_children.at(i);
        if(child->m_nodetag == tag) {
            found = true;
            m_children.removeAt(i);
            child->deleteLater();
        }
    }
    if (!found)
        qDebug() << "Node :: Child tag " << tag << " is not found, cannot delete it";
}

void Node::removeChild(Node *child) {
    bool found = false;
    Node *c = 0;
    for (int i=0; !found && i<m_children.size() ;i++) {
        c = m_children.at(i);
        if(c == child) {
            found = true;
            m_children.removeAt(i);
            child->deleteLater();
        }
    }
    if (!found)
        qDebug() << "Node :: Child Address " << child << " is not found, cannot delete it";
}


//endOf child managment methodes


void Node::setTouchEnabled(bool enabled) {
    if (!m_isTouchEnabled) {
        if (enabled) {
            //enable the touch, it was disabled and a request to enable it for this layer
            m_isTouchEnabled = true;
            connect(Director::sharedDirector()->sharedGraphicScene(),SIGNAL(signaltouchmoved(QGraphicsSceneMouseEvent*)),this,SLOT(touchMove(QGraphicsSceneMouseEvent*)));
            connect(Director::sharedDirector()->sharedGraphicScene(),SIGNAL(signaltouchreleased(QGraphicsSceneMouseEvent*)),this,SLOT(touchEnd(QGraphicsSceneMouseEvent*)));
            connect(Director::sharedDirector()->sharedGraphicScene(),SIGNAL(signaltouchpressed(QGraphicsSceneMouseEvent*)),this,SLOT(touchBegin(QGraphicsSceneMouseEvent*)));
        }
    } else {
        if (!enabled) {
            //disable the touch, and a request to disable it is sent
            m_isTouchEnabled = false;
            disconnect(Director::sharedDirector()->sharedGraphicScene(),SIGNAL(signaltouchmoved(QGraphicsSceneMouseEvent*)),this,SLOT(touchMove(QGraphicsSceneMouseEvent*)));
            disconnect(Director::sharedDirector()->sharedGraphicScene(),SIGNAL(signaltouchreleased(QGraphicsSceneMouseEvent*)),this,SLOT(touchEnd(QGraphicsSceneMouseEvent*)));
            disconnect(Director::sharedDirector()->sharedGraphicScene(),SIGNAL(signaltouchpressed(QGraphicsSceneMouseEvent*)),this,SLOT(touchBegin(QGraphicsSceneMouseEvent*)));
        }
    }
}

