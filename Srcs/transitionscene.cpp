#include "transitionscene.h"


TransitionScene::TransitionScene() {

}

TransitionScene * TransitionScene::FadeTransition(Scene *otherScene,float duration) {
    TransitionScene* newTrans = new TransitionScene;
    newTrans->m_nextScene = otherScene;
    otherScene->mainLayer->onEnterTransitionDidStart();
    newTrans->m_dur = duration;
    newTrans->m_origFade = newTrans->m_nextScene->opacity();
    newTrans->m_nextScene->setOpacity(0);
    connect(newTrans,SIGNAL(transitionDoneSignal(Scene*)),Director::sharedDirector(),SLOT(transitionDone(Scene*)));
    return newTrans;
}

void TransitionScene::setCurrScene(Scene *scene) {
    m_currScene = scene;
    m_currScene->mainLayer->onExitTransitionDidStart();
    Action* firststep = Action::fadeTo(m_dur/2,0);
    Action* gotostep2 = Action::FuncCall(this,SLOT(FadeTransition_half_done()));
    scene->runSequence(Sequence::sequenceWithActions(firststep,gotostep2,NULL));
}

void TransitionScene::FadeTransition_half_done() {
    Action* firststep = Action::fadeTo(m_dur/2,m_origFade);
    Action* gotostep3 = Action::FuncCall(this,SLOT(transition_done()));
    m_nextScene->runSequence(Sequence::sequenceWithActions(firststep,gotostep3,NULL));
}
void TransitionScene::transition_done() {
    m_currScene->mainLayer->onExitTransitionDidFinish();
    m_nextScene->mainLayer->onEnterTransitionDidFinish();
    emit transitionDoneSignal(m_nextScene);
    deleteLater();
}
